from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

class DietSnapshot(models.Model):
    name = models.CharField(max_length=150,null=True)
    portionSize = models.IntegerField(validators=[MinValueValidator(0),MaxValueValidator(3)])
    local = models.IntegerField(validators=[MinValueValidator(0),MaxValueValidator(2)])
    season = models.IntegerField(validators=[MinValueValidator(0),MaxValueValidator(2)])
    # Fruits
    id1 = models.IntegerField(verbose_name='Fruits',validators=[MinValueValidator(0),MaxValueValidator(14)])
    # Nuts and seeds
    id2 = models.IntegerField(verbose_name='Nuts and seeds',validators=[MinValueValidator(0),MaxValueValidator(14)])
    # Dried fruits
    id3 = models.IntegerField(verbose_name='Dried fruits',validators=[MinValueValidator(0),MaxValueValidator(14)])
    # Vegetables (+ potatoes)
    id4 = models.IntegerField(verbose_name='Vegetables (+ potatoes)',validators=[MinValueValidator(0),MaxValueValidator(14)])
    # Soup (ready-made, freeze-dried)
    id5 = models.IntegerField(verbose_name='Soup (ready-made, freeze-dried)',validators=[MinValueValidator(0),MaxValueValidator(14)])
    # Creams and cream specialties
    id6 = models.IntegerField(verbose_name='Creams and cream specialties',validators=[MinValueValidator(0),MaxValueValidator(14)])
    # Cheese
    id7 = models.IntegerField(verbose_name='Cheese',validators=[MinValueValidator(0),MaxValueValidator(14)])
    # Dairy products (i.e. yoghurt)
    id8 = models.IntegerField(verbose_name='Dairy products',validators=[MinValueValidator(0),MaxValueValidator(14)])
    # Dough
    id9 = models.IntegerField(verbose_name='Dough',validators=[MinValueValidator(0),MaxValueValidator(14)])
    # Pasta, rice and cereals
    id10 = models.IntegerField(verbose_name='Pasta, rice and cereals',validators=[MinValueValidator(0),MaxValueValidator(14)])
    # Industrial puff pastries, pies, pizzas, etc.
    id11 = models.IntegerField(verbose_name='Industrial puff pastries',validators=[MinValueValidator(0),MaxValueValidator(14)])
    # Snack cakes and crisps
    id12 = models.IntegerField(verbose_name='Snack cakes and crisps',validators=[MinValueValidator(0),MaxValueValidator(14)])
    # Eggs
    id13 = models.IntegerField(verbose_name='Eggs',validators=[MinValueValidator(0),MaxValueValidator(14)])
    # Cold cuts
    id14 = models.IntegerField(verbose_name='Cold cuts',validators=[MinValueValidator(0),MaxValueValidator(14)])
    # Shellfish and Molluscs
    id15 = models.IntegerField(verbose_name='Shellfish and Molluscs',validators=[MinValueValidator(0),MaxValueValidator(14)])
    # Fish
    id16 = models.IntegerField(verbose_name='Fish',validators=[MinValueValidator(0),MaxValueValidator(14)])
    # White meat
    id17 = models.IntegerField(verbose_name='White meat',validators=[MinValueValidator(0),MaxValueValidator(14)])
    # Red meat
    id18 = models.IntegerField(verbose_name='Red meat',validators=[MinValueValidator(0),MaxValueValidator(14)])
    # Cooked pulse (beans, lentils, peas, etc.)
    id19 = models.IntegerField(verbose_name='Cooked pulse',validators=[MinValueValidator(0),MaxValueValidator(14)])
    # Meat substitutes and vegetarian dishes
    id20 = models.IntegerField(verbose_name='Meat substitutes and vegetarian dishes',validators=[MinValueValidator(0),MaxValueValidator(14)])
    # Jam
    id21 = models.IntegerField(verbose_name='Jam',validators=[MinValueValidator(0),MaxValueValidator(14)])
    # Milk (cow)
    id22 = models.IntegerField(verbose_name='Cow milk',validators=[MinValueValidator(0),MaxValueValidator(14)])
    # Breakfast cereals and biscuits
    id23 = models.IntegerField(verbose_name='Breakfast cereals and biscuits',validators=[MinValueValidator(0),MaxValueValidator(14)])
    # Cakes and pastries
    id24 = models.IntegerField(verbose_name='Cakes and pastries',validators=[MinValueValidator(0),MaxValueValidator(14)])
    # Bread
    id25 = models.IntegerField(verbose_name='Bread',validators=[MinValueValidator(0),MaxValueValidator(14)])
    # Pastries and buns
    id26 = models.IntegerField(verbose_name='Pastries and buns',validators=[MinValueValidator(0),MaxValueValidator(14)])
    # Chocolates and chocolate products
    id27 = models.IntegerField(verbose_name='Chocolates and chocolate products',validators=[MinValueValidator(0),MaxValueValidator(14)])
    # Non-chocolate confectionery
    id28 = models.IntegerField(verbose_name='Non-chocolate confectionery',validators=[MinValueValidator(0),MaxValueValidator(14)])
    # Alcoholic drinks
    id29 = models.IntegerField(verbose_name='Alcoholic drinks',validators=[MinValueValidator(0),MaxValueValidator(14)])
    # Soda
    id30 = models.IntegerField(verbose_name='Soda',validators=[MinValueValidator(0),MaxValueValidator(14)])
    # Fruit juice
    id31 = models.IntegerField(verbose_name='Fruit juice',validators=[MinValueValidator(0),MaxValueValidator(14)])
    # Milk drinks (vegetable and animal) and others
    id32 = models.IntegerField(verbose_name='Milk drinks (vegetable and animal) and others',validators=[MinValueValidator(0),MaxValueValidator(14)])
    # Tea and coffee
    id33 = models.IntegerField(verbose_name='Tea and coffee',validators=[MinValueValidator(0),MaxValueValidator(14)])
    
    created_at = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.name
    objects = models.Manager()