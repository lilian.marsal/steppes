from django import forms
from .models import DietSnapshot
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from datetime import datetime

# Save the food data
class DietForm(forms.ModelForm).DateTimeField(initial=datetime.now)
    class Meta:
        model = DietSnapshot
        fields = [
            'name',
            'portionSize'
            'local',
            'season',
            'id1',
            'id2',
            'id3',
            'id4',
            'id5',
            'id6',
            'id7',
            'id8',
            'id9',
            'id10',
            'id11',
            'id12',
            'id13',
            'id14',
            'id15',
            'id16',
            'id17',
            'id18',
            'id19',
            'id20',
            'id21',
            'id22',
            'id23',
            'id24',
            'id25',
            'id26',
            'id27',
            'id28',
            'id29',
            'id30',
            'id31',
            'id32',
            'id33'
            'created at',
        ]

# User data
class UserRegistrationForm(UserCreationForm):
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = ('username','email','password1','password2')