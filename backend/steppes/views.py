from django.shortcuts import render, redirect
from .forms import DietForm
from .forms import UserRegistrationForm

def index(request):
    return render(request, 'index')

#def home_view(request):
#    return render(request, 'home.html')
def diet_view(request):
    if request.method=='POST':
        form = DietForm(request.POST)
        if form.is_valid():
            form.save()

    else:
        form = DietForm()
    return render(request, 'my_template.html',{'form': form})

# explicación de como hacerlo en chatgpt

def register(request):
    if request.method=='POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login')
        
    else:
        form = UserRegistrationForm()
    return render(request,'file path from where we obtain the info',{'form':form})
