from django.contrib import admin
from .models import DietSnapshot

class DietSnapshotAdmin(admin.ModelAdmin):
    pass


admin.site.register(DietSnapshot,DietSnapshotAdmin)