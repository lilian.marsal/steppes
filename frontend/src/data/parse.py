# -*- coding: utf-8 -*-

import json
with open("./outil_WEB.json", 'r', encoding="utf-8") as Obj:
    d = json.load(Obj)
    v = {}
    for key, value in sorted(d.items()):
        v.setdefault(value["type_name"], []).append(key)
    v = sorted(v.items(), key=lambda x : int(x[1][0]))
    v = dict(v)
    with open("./questions.json", 'w', encoding="utf-8") as file:
        v = json.dumps(v, ensure_ascii=False)
        file.write(v)