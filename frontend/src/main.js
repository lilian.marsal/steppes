import { createApp } from 'vue'
import App from './App.vue'
import router from './router'


import './assets/css/main.css'
import './assets/css/base.css'

import '@coreui/coreui/dist/js/coreui.bundle'

const app = createApp(App)

app.use(router)

app.mount('#app')
